const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();


app.use(cors());

if(process.env.NODE_ENV === 'production') {
    console.log('Mode production !');
} else {
    console.log('Mode test !');
}

app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, '../FRONTEND/public/')));

// TO DO

// Routes déployables
require('./routes')(app);
// MiddleWare pour les erreurs
app.use(function (err, req, res, next) {
	if (req.transaction) req.transaction.rollback();
	if (err.status === undefined)
		return res.status(500).json({ errorMessage: err.message });
	return res.status(err.status).json({ errorMessage: err.message });
});


app.get('*', (req, res) => {
    return res.sendFile(path.join(__dirname, '../FRONTEND/index.html'));
});

module.exports = app;