require('dotenv').config();

const {DB_USER, DB_PASS, DB_HOST, DB_PORT} = process.env;

module.exports = {
	development: {
		username: DB_USER,
		password: DB_PASS,
		database: 'nuit_info',
		host: DB_HOST,
		port: DB_PORT,
		dialect: 'mysql',
		logging: true,
		define: {
			freezeTableName: true
		}
	},
	test: {
		username: DB_USER,
		password: DB_PASS,
		database: 'nuit_info_test',
		host: DB_HOST,
		dialect: 'mysql',
		logging: false,
	},
	production: {
		username: DB_USER,
		password: DB_PASS,
		database: 'nuit_info',
		host: 'db',
		dialect: 'mysql',
		logging: false,
	},
};
