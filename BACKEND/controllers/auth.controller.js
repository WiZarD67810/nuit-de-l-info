const db = require('../models'),
	jwt = require('jsonwebtoken'),
	bcrypt = require('bcryptjs'),
	Utilisateur = db.Utilisateur;

module.exports = {
    signup: async (req, res, next) => {
		let transaction = await db.sequelize.transaction();
		req.transaction = transaction;

		try {
			// Save User to Database
			let user = await Utilisateur.create(
				{
					pseudo: req.body.pseudo,
					mot_de_passe: bcrypt.hashSync(req.body.mot_de_passe, 8),
					email: req.body.email,
                    nom: req.body.nom,
                    prenom: req.body.prenom,
				},
				{ transaction }
			);

			await transaction.commit();
			return res.json(user);
		} catch (error) {
			next(error);
		}
	},
    signin: async (req, res, next) => {
		let transaction = await db.sequelize.transaction();
		req.transaction = transaction;

		try {
			let user = await Utilisateur.findOne({
				where: {
					email: req.body.email,
				},
				transaction,
			});

			if (!user) throw new Error('User Not found.');

			let password_validation = bcrypt.compareSync(
				req.body.mot_de_passe,
				user.mot_de_passe
			);

			if (!password_validation) throw new Error('Invalid Password!');

			let token = jwt.sign({ id: user.id }, config.secret, {
				expiresIn: 86400, // 24 hours
			});

			await transaction.commit();
			return res.status(200).json({
				user: user,
				accessToken: token,
			});
		} catch (error) {
			next(error);
		}
	},
	whoami: async (req, res, next) => {
		let transaction = await db.sequelize.transaction();
		req.transaction = transaction;

		try {
			let user = await Utilisateur.findByPk(req.id, { transaction });
			if (!user) throw new Error("This user doesn't exist", 500);
			await transaction.commit();
			return res.json(user);
		} catch (error) {
			next(error);
		}
	}
}