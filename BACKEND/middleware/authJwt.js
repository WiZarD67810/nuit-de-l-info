const jwt = require('jsonwebtoken'),
	config = require('../config/auth.config.js'),
	db = require('../models'),
	User = db.User;

module.exports = {
    verifyToken: async (req, res, next) => {
		try {
			let token = await req.headers['x-access-token'];

			if (!token) throw new Error('No token provided!');

			jwt.verify(token, config.secret, (err, decoded) => {
				if (err) throw new Error('Unauthorized!');
				req.user_id = decoded.user_id;
				next();
			});
		} catch (error) {
			next(error);
		}
	},

}