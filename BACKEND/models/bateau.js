'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Bateau extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.TypeBateau, {
        foreignKey: 'type_bateau_id',
        onUpdate: 'cascade',
				onDelete: 'cascade',
      })
    }
  };
  Bateau.init({
    matricule:  {
      allowNull: false,
      type: DataTypes.STRING,
    },
    nom: {
      allowNull: false,
      type: DataTypes.STRING,
    },
  }, {
    timestamps: false,
    sequelize,
    modelName: 'Bateau',
  });
  return Bateau;
};