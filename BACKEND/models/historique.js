'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Historique extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Sauvetage, {
        foreignKey: {
          name:'sauvetage_id',
          allowNull: false
        },
        onUpdate: 'cascade',
				onDelete: 'cascade',
      });
      this.belongsTo(models.Utilisateur, {
        foreignKey: 'utilisateur_id',
        onUpdate: 'cascade',
				onDelete: 'cascade',
      });
    }
  };
  Historique.init({
    status: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    date_modif: {
      allowNull: false,
      type: DataTypes.DATE,
    },                      
  }, {
    timestamps: false,
    sequelize,
    modelName: 'Historique',
  });
  return Historique;
};