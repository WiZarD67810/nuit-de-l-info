'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Personne extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Personne.init({
    nom: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    prenom: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    date_naissance: DataTypes.DATEONLY
  }, {
    timestamps: false,
    sequelize,
    modelName: 'Personne',
  });
  return Personne;
};