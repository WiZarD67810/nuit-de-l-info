'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Sauve extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Personne, {
        foreignKey: 'personne_id',
        onUpdate: 'cascade',
				onDelete: 'cascade',
      });
      this.belongsTo(models.Sauvetage, {
        foreignKey: 'sauvetage_id',
        onUpdate: 'cascade',
				onDelete: 'cascade',
      });
      this.belongsTo(models.Bateau, {
        foreignKey: 'bateau_id',
        onUpdate: 'cascade',
				onDelete: 'cascade',
      });
    }
  };
  Sauve.init({
    personne_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    sauvetage_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    actif: {
      allowNull: false,
      defaultValue: 1,
      type: DataTypes.BOOLEAN
    },
    valide: {
      allowNull: false,
      defaultValue: 1,
      type: DataTypes.BOOLEAN
    }
  }, {
    timestamps: false,
    sequelize,
    modelName: 'Sauve',
  });
  return Sauve;
};