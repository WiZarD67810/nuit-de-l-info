'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Sauvetage extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Sauvetage.init({
    date_sauvetage: DataTypes.DATEONLY,
    nombre_victimes: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    article: {
      allowNull: false,
      type: DataTypes.TEXT('medium'),
    }
  }, {
    timestamps: false,
    sequelize,
    modelName: 'Sauvetage',
  });
  return Sauvetage;
};