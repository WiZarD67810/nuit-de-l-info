'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class TypeBateau extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  TypeBateau.init({
    nom: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    nombre_mats: DataTypes.INTEGER,
    nombre_voile: DataTypes.INTEGER,
    longueur: DataTypes.DOUBLE,
    largeur: DataTypes.DOUBLE,
    hauteur: DataTypes.DOUBLE,
    motorise: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
    },
  }, {
    timestamps: false,
    sequelize,
    modelName: 'TypeBateau',
  });
  return TypeBateau;
};