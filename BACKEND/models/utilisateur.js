'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Utilisateur extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Utilisateur.init({
    pseudo: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    mot_de_passe: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    email: {
      allowNull: false,
      unique: 'email',
      type: DataTypes.STRING,
    },
    nom: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    prenom: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    admin: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  }, {
    timestamps: false,
    sequelize,
    modelName: 'Utilisateur'
  });
  return Utilisateur;
};