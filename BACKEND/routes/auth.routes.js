const { authJwt } = require('../middleware');
const controller = require('../controllers/auth.controller');

module.exports = [
    {
		url: '/signup',
		method: 'post',
		func: controller.signup,
	},
    {
		url: '/signin',
		method: 'post',
		func: controller.signin,
	},
	{
		url: '/account',
		method: 'get',
		func: [authJwt.verifyToken, controller.whoami],
	},

]
