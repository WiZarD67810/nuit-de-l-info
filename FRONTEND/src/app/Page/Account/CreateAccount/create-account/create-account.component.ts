import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServiceService } from 'src/app/Service/authService/auth-service.service';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit {

  createInForm: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder, private route: Router, @Inject(AuthServiceService) private AuthService: AuthServiceService) { }

  ngOnInit(): void {
    this.createInForm = this.formBuilder.group({
      lastname: ['', [Validators.required, Validators.pattern(/[a-zA-Z ]{1,}/)]],
      firstname: ['', [Validators.required, Validators.pattern(/[a-zA-Z ]{1,}/)]],
      mail: ['', [Validators.required, Validators.pattern(/[a-zA-Z\.0-9]+@+[a-z]+.+[a-z]/)]],
      password: ['', [Validators.required, Validators.pattern(/[a-zA-Z ]{1,}/)]]
    });
  }

  onSubmit() {
    const lastName = this.createInForm.get('username')?.value;
    const forName = this.createInForm.get('password')?.value;

    this.AuthService.signIn(lastName, forName);
    // TODO
    this.route.navigate(["/Home"]);
  }

}
