import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';
import { AuthServiceService } from 'src/app/Service/authService/auth-service.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  authInForm: FormGroup = new FormGroup({});

  constructor(private formBuilder: FormBuilder, private route: Router, private authService: AuthServiceService, private socialAuthService: SocialAuthService) { }

  ngOnInit(): void {
    this.authInForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.pattern(/[a-zA-Z ]{1,}/)]],
      password: ['', [Validators.required, Validators.pattern(/[a-zA-Z ]{1,}/)]]
    });
  }

  onSubmit() {
    const lastName = this.authInForm.get('username')?.value;
    const forName = this.authInForm.get('password')?.value;

    this.authService.signIn(lastName, forName);
    // TODO
    this.route.navigate(["/Home"]);
  }

  loginWithGoogle() {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID)
      .then(() => {
        this.authService.signInGoogle();
        this.route.navigate(['/Home'])
      });
  }

  loginWithFacebook() {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID)
      .then(() => {
        this.authService.signInFacebook();
        this.route.navigate(['/Home'])
      });
  }

}
