import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/Models/user.model';
import { AuthServiceService } from 'src/app/Service/authService/auth-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private AuthService: AuthServiceService) { }

  ngOnInit(): void {
  }
  
  public get isAuth() : boolean {
    return this.AuthService.isAuth;
  }

  
  public get user() : User {
    return this.AuthService.User;
  }
  

  public logout() {
    this.AuthService.signOut();
  }
}
