import { Routes } from "@angular/router"
import { ConnexionComponent } from "../Page/Account/connexion/connexion.component";
import { CreateAccountComponent } from "../Page/Account/CreateAccount/create-account/create-account.component";
import { HomeAdminComponent } from "../Page/Admin/Home/home-admin/home-admin.component";
import { EasterEggComponent } from "../Page/EasterEgg/easter-egg/easter-egg.component";
import { HomeComponent } from "../Page/Home/home/home.component"
import { NotFoundComponent } from "../Page/Not-Found/not-found/not-found.component"
import { CommunauteComponent } from "../Page/communaute/communaute.component";

const appRoutes: Routes = [
    {
        path: "Home",
        component: HomeComponent,
    },
    {
        path: "Login",
        component: ConnexionComponent,
    },
    {
        path: "CreateAccount",
        component: CreateAccountComponent,
    },
    {
        path: "EasterEgg",
        component: EasterEggComponent,
    },
    {
        path: "Communaute",
        component: CommunauteComponent,
    },
    {
        path: "Not-Found",
        component: NotFoundComponent,
    },
    // Admin
    {
        path: "Admin/Home",
        component: HomeAdminComponent,

    },
    {
        path: "",
        component: HomeComponent,
    },
    {
        path: "**",
        redirectTo: "/Not-Found"
    }
];

export default appRoutes;