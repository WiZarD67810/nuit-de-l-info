import { Injectable } from '@angular/core';
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { User } from 'src/app/Models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {

  isAuth = false;
  User: User = new User("","");

  constructor(private socialAuthService: SocialAuthService) { }

  signIn(username: string, password: string) {
    this.isAuth = true;
  }

  signOut() {
    this.isAuth = false;
    this.User = new User("","");
    this.socialAuthService.signOut();
  }

  signInGoogle() {
    this.socialAuthService.authState.subscribe((user) => {
      this.User = new User(user.firstName, user.lastName);
      this.isAuth = (user != null);
    });
  }

  signInFacebook() {
    this.socialAuthService.authState.subscribe((user) => {
      this.User = new User(user.firstName, user.lastName);
      this.isAuth = (user != null);
    });
  }
}
