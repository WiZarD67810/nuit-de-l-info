import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Page/Common/header/header.component';
import { FooterComponent } from './Page/Common/footer/footer.component';
import { HttpClientModule } from '@angular/common/http';
import { NotFoundComponent } from './Page/Not-Found/not-found/not-found.component';
import { HomeComponent } from './Page/Home/home/home.component';
import { RouterModule } from '@angular/router';

import appRoutes from './Routes/routerConfig';
import { ConnexionComponent } from './Page/Account/connexion/connexion.component';
import { HomeAdminComponent } from './Page/Admin/Home/home-admin/home-admin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateAccountComponent } from './Page/Account/CreateAccount/create-account/create-account.component';
import { FacebookLoginProvider, GoogleLoginProvider, SocialLoginModule } from 'angularx-social-login';
import { EasterEggComponent } from './Page/EasterEgg/easter-egg/easter-egg.component';
import { CommunauteComponent } from './Page/communaute/communaute.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NotFoundComponent,
    HomeComponent,
    ConnexionComponent,
    HomeAdminComponent,
    CreateAccountComponent,
    EasterEggComponent,
    CommunauteComponent
  
  ],
  imports: [
    BrowserModule, 
    FormsModule, 
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    SocialLoginModule
  ],
  providers: [{
    provide: 'SocialAuthServiceConfig',
    useValue: {
      autologin: false,
      providers: [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider("270447603752-nnf35bcac4eepsikdu2jnehnk6pn522r.apps.googleusercontent.com")
        },
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider('1343712666062082')
        }
      ]
    }
  }],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { } 
